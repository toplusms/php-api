<?

$username = "username";
$password = md5( "password" );

function makeApiCall( $method, $cmd, $postData, $username, $password ) {
    $ch = curl_init();

    curl_setopt( $ch, CURLOPT_URL, 'https://panel.toplusms.tc/api/v1/' . $cmd );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
    curl_setopt( $ch, CURLOPT_HEADER, 1 );
    curl_setopt( $ch, CURLOPT_POST, intval( $method == "post" ) );
    curl_setopt( $ch, CURLOPT_USERPWD, $username . ":" . $password );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $postData );
    curl_setopt( $ch, CURLOPT_TIMEOUT, 90 );
    $result = curl_exec( $ch );

    $header_size = curl_getinfo( $ch, CURLINFO_HEADER_SIZE );
    $header = substr( $result, 0, $header_size );
    $body = substr( $result, $header_size );

    curl_close( $ch );

    return array(
        "header" => $header,
        "body" => $body 
    );
}

// normal sms gönder
$postData = array();
$postData["caption"] = "DEMO";
$postData["validity"] = 2;
$postData["message"] = "Örnek mesaj denemesidir.";
$postData["senddate"] = date("Y-m-d H:i:s");
$postData["numbers"] = "5551112233,5552223344,555334455,5374937766,5538356060";
$postData["encoding"] = "tr";

$apiResult = makeApiCall( "post", "sms/send/normal", $postData, $username, $password );

echo "Headerlar: ";
print_r( $apiResult["header"] );

echo "JSON Cevap: ";
print_r( $apiResult["body"] );

// rapor sorgulama
sleep( 5 );
$queueId = json_decode( $apiResult["body"] )->queueId;
$apiResult = makeApiCall( "post", "sms/reports/" . $queueId, null, $username, $password );

echo "Headerlar: ";
print_r( $apiResult["header"] );

echo "JSON Cevap: ";
print_r( $apiResult["body"] );

// kredi sorgulama
sleep( 5 );
$apiResult = makeApiCall( "post", "user/profileDetails", null, $username, $password );

echo "Headerlar: ";
print_r( $apiResult["header"] );

echo "JSON Cevap: ";
print_r( $apiResult["body"] );


